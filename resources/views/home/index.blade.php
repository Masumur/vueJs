@extends('master.main')
@section('title') Vue Js Blog @endsection
@section('content')
<!--Main layout-->
<main class="mt-5 pt-5">
    <div class="container">

        <!--Section: Jumbotron-->
        <section class="card wow fadeIn" style="background-image: url(https://mdbootstrap.com/img/Photos/Others/gradient1.jpg); visibility: visible; animation-name: fadeIn;">

            <!-- Content -->
            <div class="card-body text-white text-center py-5 px-5 my-5">

                <h1 class="mb-4">
                    <strong>Learn Bootstrap 4 with MDB</strong>
                </h1>
                <p>
                    <strong>Best &amp; free guide of responsive web design</strong>
                </p>
                <p class="mb-4">
                    <strong>The most comprehensive tutorial for the Bootstrap 4. Loved by over 500 000 users. Video and written
                        versions available. Create your own, stunning website.</strong>
                </p>
                <a target="_blank" href="https://mdbootstrap.com/bootstrap-tutorial/" class="btn btn-outline-white btn-lg waves-effect waves-light">Start free tutorial
                    <i class="fa fa-graduation-cap ml-2"></i>
                </a>

            </div>
            <!-- Content -->
        </section>
        <!--Section: Jumbotron-->

        <hr class="my-5">

        <!--Section: Cards-->
        <section class="text-center" id="post">

            <home-link ></home-link>

            <!--Pagination-->
                <nav class="d-flex justify-content-center wow fadeIn" style="visibility: visible; animation-name: fadeIn;">
                <ul class="pagination pg-blue">

                    <!--Arrow left-->
                    <li class="page-item disabled">
                        <a class="page-link waves-effect waves-effect" href="https://mdbootstrap.com/previews/free-templates/blog/home-page.html#" aria-label="Previous">
                            <span aria-hidden="true">«</span>
                            <span class="sr-only">Previous</span>
                        </a>
                    </li>

                    <li class="page-item active">
                        <a class="page-link waves-effect waves-effect" href="https://mdbootstrap.com/previews/free-templates/blog/home-page.html#">1
                            <span class="sr-only">(current)</span>
                        </a>
                    </li>
                    <li class="page-item">
                        <a class="page-link waves-effect waves-effect" href="https://mdbootstrap.com/previews/free-templates/blog/home-page.html#">2</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link waves-effect waves-effect" href="https://mdbootstrap.com/previews/free-templates/blog/home-page.html#">3</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link waves-effect waves-effect" href="https://mdbootstrap.com/previews/free-templates/blog/home-page.html#">4</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link waves-effect waves-effect" href="https://mdbootstrap.com/previews/free-templates/blog/home-page.html#">5</a>
                    </li>

                    <li class="page-item">
                        <a class="page-link waves-effect waves-effect" href="https://mdbootstrap.com/previews/free-templates/blog/home-page.html#" aria-label="Next">
                            <span aria-hidden="true">»</span>
                            <span class="sr-only">Next</span>
                        </a>
                    </li>
                </ul>
            </nav>
            <!--Pagination-->

        </section>
        <!--Section: Cards-->

    </div>
</main>
<!--Main layout-->
@endsection

@section('script')
    <script src="{{ mix('js/post.js')}} "></script>
@endsection
