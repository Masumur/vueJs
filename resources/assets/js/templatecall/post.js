import Vue from 'vue'
import Post from './../components/post.vue'


window.onload = function () {
    const app = new Vue({
        el: '#post',
        components: {
            HomeLink : Post
        }
    });
}